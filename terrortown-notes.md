# Terrortown notes

## Races in lore:
Enemies vs. playable races?
- werewolf
- human
- lizardman?
- undead
- vampire
- skeleton?

## Skills
### Magic damage skills [int/cunning]:
- Fire? wind? (strong inaccurate aoe)
- Earth (strong inaccurate aoe)
- Water (weak accurate aoe)
- light (weak accurate aoe)
- Lightning (weak accurate st)
- psychic/mind control? (Weak accurate st)
- disease (dot aoe)
- darkness (dot aoe)
- blood? (Dot st)
- fire? (Dot st)
- ice? (Strong inaccurate st)
- fire? (Strong inaccurate st)

### Physical damage skills [strength/dex]:
- 2h axes (strong inaccurate aoe)
- 2h maces (strong inaccurate aoe)
- 2h swords (weak accurate aoe)
- lances/spears (weak accurate aoe)
- 1h swords (weak accurate st)
- 1h maces (Weak accurate st)
- fire bombs? Bombs? (dot aoe)
- poisons? (dot aoe)
- poisons? Daggers? (Dot st)
- bows? (Dot st)
- 1h axes (Strong inaccurate st)
- crossbows (Strong inaccurate st)

## Nondamage skills
[Magic]~[physical]. Crafting split into different goals.
- self healing (spells~bandages or medicine)
- team healing (spells~bandages or medicine)
- cc (spells~physical action)

- buffs (spells[int]~potions)
- debuffs (spells[cun]~poison)
- shielding? (spells~big shields)
- taunting (mind control~warcries)
- crafting (magically~physically)
- stealth (spells~physical action)
- theft/looting (?~physically)
- allies (summoning~hiring/followers) [maybe make this its own thing, a companion]
- ? (?~?) [makes even number if keep companion 11 -> 12, just like damage skills. thatd be nice.]

### attributes
- intellect [magic damage]
- cunning [magic accuracy]
- strength [physical damage]
- dexterity [physical accuracy]
Nondamage skills each boosted by 1 specified attribute

### classes
So you choose 1,2,3 attributes, decreasing in power sequentially. Major, minor, tertiary. 24 options.

- Warrior [str-dex-cun]
- Blademaster [str-dex-int]
- Berserker [str-cun-dex]
- Sniper [dex-str-cun]
- Archer [dex-str-int]
- Ranger [dex-int-str]
- Scout [dex-int-cun]
- Tracker (dont love this one) [dex-cun-str]
- Hunter [dex-cun-int]
- Stalker [cun-int-dex]
- Rogue [cun-str-int]
- Thief [cun-dex-int]
- Assassin [cun-dex-str]
- Ninja [cun-str-dex]
- Shadow [cun-int-str]
- Death Knight [str-cun-int]
- Spellsword [str-int-cun]
- Paladin [str-int-dex]
- Acolyte [int-dex-str]
- Shaman [int-str-dex]
- Druid [int-str-cun]
- Elementalist [int-dex-cun]
- Warlock [int-cun-str]
- Sorcerer [int-cun-dex]

12 damage roles, somehow itll fit perfectly! i think we will do it by what they DONT get, cause in toontown everybody had everything. maybe they cannot use 2 from each category? same role or different?
too many archers when there arent that many bows in the game. need to change them to more melee ranger oriented, or swordsman type names. move blademaster to dex maybe.
small numbers or big numbers? or decide that later once skeleton is up and running? once we need to kill our first enemy, have to give it some hp. im thinking small numbers to stay true to toontown. yeah, and life will be level as well.

---

so basically toontown was a turn based MMORPG. you could have up to 4 people in 1 battle. monsters roamed the overworld, and players initiated battles with monsters by walking up to them. players could skip battles pretty easily as the mobs just walked in straight lines. this will be changed in terrortown and the mobs will be a bit more aggressive, kind of like WoW or something where you CAN skip battles its just harder.

the battles were dynamic; other players would see you battling in the street and could join in. you could also text chat and yell out HEY HELP COME HERE BRO! and they would pop up in cute chat bubbles. another awesome feature is that the monsters would "take over" buildings in the streets of toontown, creating lil mini dungeons. the cute toon buildings would change from lil houses called "Cheesey Crackers" whatever to big black industrial buildings that said "CHEESEY CRACKERS, INC." it was just a really soulful feature. the dungeons were very hard to complete alone, depending on the number of "floors" of the building. so players would sit outside the buildings waiting for others to join! these types of features kind of force player interaction, like the old world quests in wow.

the first thing i want to design and already spent time designing in the file that i cant find is the class system. in toontown, there were 7 different types of attacks that you could use on your turn. players all picked at same time and you could see what your teammates picked or were "hovering" live as they selected. in the beginning of the game you start with 2 basic damaging attacks, then get access to more with varying effects as you level up. BUT big caveat is you can only pick 6 out of the 7. so that got me thinking about another game, Grim Dawn, with a multiclass system. in grim dawn you choose 2 base classes, i.e. fighter + mage, and you get a cooler name like battlemage. So terrortown is going to be something like that; you spec into x number of types of attacks and that determines the cool name of your class.

this is the attack screen from toontown, one row would be greyed out for every player. there were heals, single target damages, aoe damages, and minor CC
[image]

so in terrortown you will start as "Recruit" or whatever with only access to 2 basic single target damage types, then as you level up you spec into newer ones, and your class name changes over time

theres also a big focus on player customization and socialization. you can buy gifts for other players and you unlock a hideout or homebase type thing that you can buy decorations for. TONS of cosmetics. the character creator will be very in depth, with lots of fantasy races. The name Terrortown is kind of like, the lore is like basic dark fantasy with werewolves, trolls/ogres, vampires, etc

toontown was just such a unique game with so many features full of soul. the reward for one of the endgame raids was a consumable that spawned a bunch of ingame currency for everyone in your vicinity. so they would have "Beanfests" in the final town of the game where everybody would stand in a circle and use these consumables for lots of jellybeans (the currency). thats how i got all my cute outfits
