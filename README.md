## Terrortown
Spiritual successor to Disney's Toontown Online built in Unity. Horror/Halloween world, a turn-based online RPG with dynamic battling; you can join other people's battles and see them happening in the world. Other notable features of Toontown present as well, including enemies taking over parts of the map, small damage numbers, life points used as character level, etc. etc.

My first foray into game development, so wish me luck.
